# Step 3

## Add Grid to body content

Inside the main tags lets apply a basic grid layout to the content.

    <main class="mdl-layout__content">
      <div class="mdl-grid">
        <div class="mdl-cell mdl-cell--12-col">
          <p>...</p>
        </div>
        <div class="mdl-cell mdl-cell--8-col-tablet">
          <p>...</p>
        </div>
        <div class="mdl-cell mdl-cell--4-col-phone">
          <p>...</p>
        </div>
        <div class="mdl-cell mdl-cell--4-col-phone">
          <p>...</p>
        </div>
        <div class="mdl-cell mdl-cell--2-col-phone">
          <p>...</p>
        </div>
        <div class="mdl-cell mdl-cell--2-col-phone">
          <p>...</p>
        </div>
      </div>
    </main>

# Adding a footer

Lets also add a footer to the page while we are at it.  Just before the closing
main tag add the following:

    <footer class="mdl-mini-footer">
      <div class="mdl-mini-footer__left-section">
        <div class="mdl-logo">MDL Demo</div>
      </div>
      <div class="mdl-mini-footer__right-section">
        <ul class="mdl-mini-footer__link-list">
          <li><a href="#">Help</a></li>
          <li><a href="#">Privacy & Terms</a></li>
        </ul>
      </div>
    </footer>
