# Step 2

## Add basic page layout

In _index.html_ lets add a basic navigation and page layout within the body tags, change the contents of the body tags to the following (creating the outline structure first)

    <div>
      <header>
        <div>
          <span>GDG Gigcity Material Design Lite</span>
        </div>
        <div>
          <nav>
            <a href="#">Home</a>
            <a href="#">Blog</a>
            <a href="#">About</a>
            <a href="#">Contact</a>
          </nav>
        </div>
      </header>
      <div>
        <nav>
          <a href="#">Home</a>
          <a href="#">Blog</a>
          <a href="#">About</a>
          <a href="#">Contact</a>
        </nav>
      </div>
      <main>
        <!-- Content goes here -->
      </main>
    </div>

With the basic structure in place lets go back and add classes so the layout
will actually be styled.

    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
      <header class="mdl-layout__header mdl-layout__header--waterfall demo-header">
        <div class="mdl-layout__header-row demo-logo-row">>
          <span class="mdl-layout__title">GDG Gigcity Material Design Lite</span>
        </div>
        <div class="mdl-layout__header-row demo-navigation-row mdl-layout--large-screen-only">
          <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
            <a href="#" class="mdl-navigation__link is-active">Home</a>
            <a href="# class="mdl-navigation__link"">Blog</a>
            <a href="#" class="mdl-navigation__link">About</a>
            <a href="#" class="mdl-navigation__link">Contact</a>
          </nav>
        </div>
      </header>
      <div class="mdl-layout__drawer mdl-layout--small-screen-only">
        <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">>
          <a href="#" class="mdl-navigation__link is-active">Home</a>
          <a href="#" class="mdl-navigation__link">Blog</a>
          <a href="#" class="mdl-navigation__link">About</a>
          <a href="#" class="mdl-navigation__link">Contact</a>
        </nav>
      </div>
      <main class="mdl-layout__content">
        <!-- Content goes here -->
      </main>
    </div>

