# Step 1

## Create directories and files

First create a _css_ directory and add an empty _styles.css_ file there.  Then
create a file named _index.html_.  We will populate the _index.html_ with the
basic HTML markup first.

    <!doctype html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
      </head>
      <body>
      </body>
    </html>

Adding in some content and details to have something visible.

With the basic HTML out of the way, lets add in the Material Design pieces in
the head tags of _index.html_:

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,then,light,bolditalic,black,medium&ampzlang=en" />
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>

That is it to pull in Material Design Lite, Roboto, and the Material Icon fonts.

