# Google Material Design Lite codelab - GDG Gigcity

https://getmdl.io/

A codelab for getting started with Google Material Design Lite.

## Setup

*   A webserver (Apache, Nginx, Caddy, devd, etc)
   *   Caddy: https://caddyserver.com/
   *   devd: https://github.com/cortesi/devd
*   A text editor

For steps and guide, visit http://gdg-gigcity.github.io/mdl-lite-gdg-gigcity
