# Step 4

# Add cards

For each of the `<div class="mdl-cell ...">` we will change them to the
following:

    <div class="mdl-cell mdl-card mdl-shadow--4dp demo-card">
      <div class="mdl-card__media mdl-card--expand"><div>
      <div class="mdl-card__supporting-text">
      ...
      </div>
      <div class="mdl-card__actions mdl-card--border">
        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect mdl-button--accent">
          read more
        </a>
      </div>
    </div>
